package com.company.model;

public class Exchanger {
    private String name = "Меняйка";;
    private float currencyOfUsd = 28.7f;
    private int limitInUsd = 20_000;

    public Exchanger(String name, float currencyOfUsd, int limitInUsd) {
        this.name = name;
        this.currencyOfUsd = currencyOfUsd;
        this.limitInUsd = limitInUsd;
    }

    public String getName() {
        return name;
    }

    public float getCurrencyOfUsd() {
        return currencyOfUsd;
    }

    public int getLimitInUsd() {
        return limitInUsd;
    }
}
