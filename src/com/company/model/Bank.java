package com.company.model;

public class Bank {

  private String name = "Приват";
  private float currencyOfUsd = 28.7f;
  private int limitInUah = 150_000;

   public Bank(String name, float currencyOfUsd, int limitInUah) {
      this.name = name;
      this.currencyOfUsd = currencyOfUsd;
      this.limitInUah = limitInUah;
   }
   public String getName() {
      return name;
   }

   public float getCurrencyOfUsd() {
      return currencyOfUsd;
   }

   public int getLimitInUah() {
      return limitInUah;
   }
}
