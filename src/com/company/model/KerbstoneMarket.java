package com.company.model;

public class KerbstoneMarket {
    private String name = "Рынок";
    private float currencyOfUsd = 26.8f;

    public KerbstoneMarket(String name, float currencyOfUsd) {
        this.name = name;
        this.currencyOfUsd = currencyOfUsd;
    }
    public String getName() {
        return name;
    }

    public float getCurrencyOfUsd() {
        return currencyOfUsd;
    }

}
